package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import perso.Carte;
import perso.Deck;
import perso.ErreurTypeInconnu;
import perso.Jeu;

public class DeckTest {

	@Test
	public void testConstructeurR() {
		try {
			Deck test = new Deck("rouleur");
			ArrayList<Carte> paquet=new ArrayList<Carte>();
			for(int i=3; i<=7; i++) {
				for(int j=0; j<3; j++) {
					paquet.add(new Carte(i));
				}
			}
			assertEquals("le paquet devrait etre rempli ",paquet,test.getCartes());
			assertEquals("la main devrait etre vide ",new ArrayList<Carte>(),test.getMain());
			assertEquals("la defausse devrais etre vide ",new ArrayList<Carte>(),test.getDefausse());
		} catch (ErreurTypeInconnu e) {
			e.printStackTrace();
		}
	}
	@Test
	public void testConstructeurS() {
		try {
			Deck test = new Deck("sprinter");
			ArrayList<Carte> paquet=new ArrayList<Carte>();
			for(int i=2; i<=5; i++) {
				for(int j=0; j<3; j++) {
					paquet.add(new Carte(i));
				}
			}
			for(int i=0; i<3; i++) {
				paquet.add(new Carte(9));
			}
			assertEquals("le paquet devrait etre rempli ",paquet,test.getCartes());
			assertEquals("la main devrait etre vide",new ArrayList<Carte>(),test.getMain());
			assertEquals("la defausse devrais etre vide ",new ArrayList<Carte>(),test.getDefausse());
		} catch (ErreurTypeInconnu e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testAjouterFatigue() {
		try {
			Deck test = new Deck("rouleur");
			ArrayList<Carte> paquet=new ArrayList<Carte>();
			for(int i=3; i<=7; i++) {
				for(int j=0; j<3; j++) {
					paquet.add(new Carte(i));
				}
			}
			assertEquals("le paquet devrait etre rempli ",paquet,test.getCartes());
			assertEquals("la main devrait etre vide ",new ArrayList<Carte>(),test.getMain());
			assertEquals("la defausse devrais etre vide ",new ArrayList<Carte>(),test.getDefausse());
		} catch (ErreurTypeInconnu e) {
			e.printStackTrace();
		}
	}
	
	
}
