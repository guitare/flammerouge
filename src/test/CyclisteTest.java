package test;

import static org.junit.Assert.*;

import org.junit.Test;

import perso.Cycliste;
import perso.Deck;
import perso.Jeu;
import perso.Rouleur;

public class CyclisteTest {

	@Test
	public void testConstructeurSprinteur() {
		Cycliste test=new Cycliste("Sprinter");
		assertEquals("le type devrais etre Sprinter","Sprinter",test.getType());
		assertEquals("le deck devrais etre null",null,test.getCartes());
		assertEquals("x devrais etre 0",0,test.getX());
		assertEquals("le nom devrais etre S1","S1",test.toString());
	}
	@Test
	public void testConstructeurRouleur() {
		Cycliste test=new Cycliste("Rouleur");
		assertEquals("le type devrais etre Rouleur","Rouleur",test.getType());
		assertEquals("le deck devrais etre null",null,test.getCartes());
		assertEquals("x devrais etre 0",0,test.getX());
		assertEquals("le nom devrais etre R4","R4",test.toString());
	}
	@Test
	public void testConstructeurChaineErone() {
		Cycliste test=new Cycliste("jgfuy");
		assertEquals("le type devrais etre null",null,test.getType());
		assertEquals("le deck devrais etre null",null,test.getCartes());
		assertEquals("x devrais etre 0",0,test.getX());
		assertEquals("le nom devrais etre null",null,test.toString());
	}
	
	@Test
	public void testPlacer() {
		Jeu testj = new Jeu();
		Cycliste test=new Cycliste("Rouleur");
		Jeu j = new Jeu();
		assertEquals("la methode devrais retourner un boolean vrais",true,test.placer(5));
		assertEquals("x devrais etre 5",5,test.getX());
	}
	@Test
	public void testPlacerNégatif() {
		Jeu testj = new Jeu();
		Cycliste test=new Cycliste("Rouleur");
		Jeu j = new Jeu();
		assertEquals("la methode devrais retourner un boolean faux",false,test.placer(-5));
		assertEquals("x devrais etre 0",0,test.getX());
	}
	@Test
	public void testPlacerNull() {
		Cycliste test=new Cycliste("Rouleur");
		Jeu j = new Jeu();
		assertEquals("la methode devrais retourner un boolean vrais",true,test.placer(0));
		assertEquals("x devrais etre 0",0,test.getX());
	}
	
	@Test
	public void testAvancer() {
		Cycliste test=new Cycliste("Rouleur");
		Jeu j = new Jeu();
		assertEquals("la methode devrais retourner un boolean faux",false,test.avancer(5));
		assertEquals("x devrais etre 5",5,test.getX());
	}
	@Test
	public void testAvancerFini() {
		Cycliste test=new Cycliste("Rouleur");
		Jeu j = new Jeu();
		test.placer(78);
		assertEquals("la methode devrais retourner un boolean vrais",true,test.avancer(5));
		assertEquals("x devrais etre 78",78,test.getX());
	}
	
	
	
}
