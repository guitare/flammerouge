package perso;

/**
 * Classe principale du jeu Flamme Rouge
 * @author Gaetan Henry, Ulysse Kaba
 */
public class Principale {

	/**
	 * Methode main executee par java
	 * @param args
	 */
	public static void main(String[] args) {
		Jeu jeu = new Jeu();
		jeu.initialiser();
		jeu.lancer();
	}

}
