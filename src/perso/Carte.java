package perso;

/**
 * Classe de definition des cartes utilisees dans le jeu
 * @author Gaetan Henry, Ulysse Kaba
 */
public class Carte {
	private int valeur;
	
	/**
	 * Constructeur de carte
	 * @param v Valeur de la carte (peut �tre n'importe quel entier)
	 */
	public Carte(int v) {
		this.valeur=0;
		if(v>0) {
			this.valeur = v;
		}
	}
	
	/**
	 * Getter pour la valeur de la carte
	 * @return valeur de la carte
	 */
	public int getVal() {
		return this.valeur;
	}
	
	/**
	 * toString de la carte
	 * @return valeur de la carte sous forme de chaine de caracteres
	 */
	public String toString() {
		return valeur+"";
	}
	
}
