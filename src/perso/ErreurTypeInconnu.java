package perso;

/**
 * Classe representant l'erreur lancee lorsque le type de Cycliste demande n'existe pas
 * @author Gaetan Henry, Ulysse Kaba
 */
@SuppressWarnings("serial")
public class ErreurTypeInconnu extends Exception {

	/**
	 * Constructeur simple
	 */
	public ErreurTypeInconnu() {
		super("Le type demande n'existe pas");
	}
}
