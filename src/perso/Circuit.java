package perso;

/**
 * Classe modelisant le circuit utilise durant la partie
 * @author Gaetan Henry, Ulysse Kaba
 */
public class Circuit {
	private Cycliste[][] trace;
	private String[] deniv;
	
	/**
	 * Constructeur vide
	 * Construit le circuit suivant le modele "La Classicissima" du jeu physique
	 */
	public Circuit() {
		this.trace = new Cycliste[80][2];
		this.deniv = new String[80];
		
		for(int i=0; i<deniv.length; i++) {
			deniv[i] = "Plat";
		}
		for(int i=15;i<25; i++) {
			deniv[i] = "Montee";
		}
		for(int i=25; i<29; i++){
			deniv[i] = "Descente";
		}
		for(int i=40; i<45; i++) {
			deniv[i] = "Montee";
		}
		for(int i=45; i<49; i++) {
			deniv[i] = "Descente";
		}
		for(int i=54; i<57; i++) {
			deniv[i] = "Montee";
		}
		for(int i=57; i<=60; i++) {
			deniv[i] = "Descente";
		}
	}
	/**
	 * Constructeur pour un circuit personnalise
	 * @param chaine Chaine de caractere decrivant le circuit a creer
	 * @throws Error Dans le cas o� le circuit demande est trop petit
	 */
	public Circuit(String chaine) throws Error{
		if(chaine == null || chaine.length()<40) {
			throw new Error("Circuit trop petit");
		}else {
			this.trace = new Cycliste[chaine.length()][2];
			this.deniv = new String[chaine.length()];
			int compt = 0;
			for(int i=0; i<chaine.length(); i++) {
				char c = chaine.charAt(i);
				switch(c) {
				case 'p' : 
					deniv[i] = "Plat";
					break;
				case 'm' : 
					deniv[i] = "Montee";
					break;
				case 'd' :
					deniv[i] = "Descente";
					break;
				default : 
					compt++;
				}
			}
			if(compt>0) {
				System.out.println(compt+" caractere n'ont pas ete reconnus dans le fichier contenant le circuit, ils ont donc ete ignores");
				
			}
		}
		
	}
	
	/**
	 * Methode de placement d'un cycliste sur une case du circuit
	 * @param c Cycliste a placer sur la case
	 * @param x Abscisse de la case voulue dans le tableau
	 * @return vrai si le placement a eu lieu, faux si la place est prise
	 */
	public boolean placerCycliste(Cycliste c, int x) {
		boolean reussi = false;
		if(estLibre(x)){
			if(trace[x][0]==null){
				trace[x][0]=c;
				reussi=true;
			}else{
				trace[x][1]=c;
				reussi=true;
			}
		}else{
			while(!estLibre(x)){
				x--;
			}
			if(trace[x][0]==null){
				trace[x][0]=c;
				reussi=true;
			}else{
				trace[x][1]=c;
				reussi=true;
			}
		}
		return reussi;
	}
	
	/**
	 * Permet de placer un cycliste sur le circuit pour la premiere fois
	 * @param c Cycliste a placer
	 * @param i position en x o� placer le cycliste
	 * @return vrai si le placement a eu lieu, faux sinon
	 */
	public boolean placerPremier(Cycliste c,int i){
		boolean res=false;
		if(trace[i][0]==null){
			trace[i][0]=c;
			res=true;
		}else{
			trace[i][1]=c;
			res=true;
		}
		return res;
	}
	
	/**
	 * Methode indiquant si une place est libre a l'abscisse demandee
	 * @param x Abscisse de la case a tester
	 * @return vrai si une place est libre, faux sinon
	 */
	public boolean estLibre(int x){
		boolean res = false;
		if(x<Jeu.circ.getTrace().length){
			if(trace[x][0]==null || trace[x][1]==null) {
				res=true;
			}
		}
		return res;
	}
	 
	/**
	 * Methode permettant de liberer une place dans le circuit
	 * @param c Cycliste a retirer du circuit
	 */
	public void liberer(Cycliste c) {
		for(int i=0;i<trace.length;i++){
			for(int j=0;j<2;j++){
				if(c==trace[i][j]){
					trace[i][j]=null;
					i=trace.length;
					break;
				}
			}
		}
		
	}
	
	/**
	 * Methode toString permettant l'affichage du Circuit a n'importe quel moment
	 * @return Chaine de caractere decrivant l'etat du circuit
	 */
	public String toString() {
		String res = "";
		int taille = trace.length;
		for(int i=0; i<taille;i++) {
			res+="====";
		}
		res+="\n";
		for(int j=0;j<2;j++) {
			for(int i=0; i<taille; i++) {
				if(deniv[i].compareTo("Montee")==0){
					if(trace[i][j]==null) {
						res+="/../";
					}else {
						res+="/"+trace[i][j]+"/";
					}
				}else if(deniv[i].compareTo("Descente")==0) {
					if(trace[i][j]==null) {
						res+="\\..\\";
					}else {
						res+="\\"+trace[i][j]+"\\";
					}
				}else {
					if(trace[i][j]==null) {
						res+="|..|";
					}else {
						res+="|"+trace[i][j]+"|";
					}
				}
			}
			res+="\n";
		}
		for(int i=0; i<taille; i++) {
			res+="====";
		}
		return res;
		
	}
	
	/**
	 * Methode derivee de toString ne prenant en compte que les 5 premiere cases du circuit
	 * (utile pour afficher le placement des joueurs pendant la phase de preparation)
	 * @return Chaine de caracteres decrivant l'etat des 5 premieres cases du circuit
	 */
	public String departToString() {
		String res = "";
		
		for(int i=0; i<5; i++) {
			res+="=c"+(i+1)+"=";
		}
		res+="\n";
		for(int j=0; j<2; j++) {
			for(int i=0; i<5; i++) {
				if(trace[i][j]==null) {
					res+="|..|";
				}else {
					res+="|"+trace[i][j]+"|";
				}
			}
			res+="\n";
		}
		for(int i=0; i<5; i++) {
			res+="=c"+(i+1)+"=";
		}
		
		return res;
	}
	
	/**
	 * Methode permettant de detecter si des cyclistes sont eligibles a l'aspiration ou non
	 * @return vrai si il reste des cyclistes pouvant beneficer des l'aspiration
	 */
	public boolean aspirationPossible(){
		boolean res=false;
		for(int i=0;i<trace.length-2;i++){
			if(trace[i][0]!=null && trace[i+1][0]==null && trace[i+2][0]!=null){
				res=true;
			}
		}
		return res;
	}
	
	/**
	 * Getter du trace du circuit
	 * @return tableau des cyclistes presents sur le circuit
	 */
	public Cycliste[][] getTrace(){
		return(trace);
	}
	/**
	 * Getter du denivele du circuit
	 * @return Tableau de chaines decrivant l'aspect de chacune des cases du circuit
	 */
	public String[] getDeniv(){
		return(deniv);
	}
	
}
