package perso;
import java.io.*;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Classe servant a gerer les interactions avec l'utilisateur
 * Permet la sauvegarde et la lecture de fichiers
 * Permet de demander une valeur numerique a l'utilisateur tout en gerant les eventuels problemes
 * @author Gaetan Henry, Ulysse Kaba
 */
public class Interactions {
	private String destination;
	private Scanner sc;
	
	/**
	 * Constructeur prenant le nom du fichier utilise pour la sauvegarde
	 * @param dest Nom du fichier utilise pour la sauvegarde de la derniere partie jouee
	 */
	public Interactions(String dest) {
		sc = new Scanner(System.in);
		this.destination = dest;
		try {
			BufferedWriter fich = new BufferedWriter(new FileWriter(destination));
			fich.write("Debut de la partie : ");
			fich.newLine();
			fich.close();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Methode d'enregistrement d'une chaine de caractere dans le fichier de sauvegarde
	 * @param texte Chaine de caractere a ajouter dans le fichier de sauvegarde
	 */
	public void ecrire(String texte) {
		try {
			BufferedWriter fich = new BufferedWriter(new FileWriter(destination,true));
			fich.write(texte);
			fich.newLine();
			fich.close();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Methode de chargement d'un circuit depuis un fichier texte
	 * @param src Nom du fichier a utiliser pour la generation du circuit
	 * @return Chaine de caractere decrivant le circuit a generer
	 */
	public String chargerCircuit(String src) {
		String res = "";
		try {
			BufferedReader fich = new BufferedReader(new FileReader(src));
			res = fich.readLine();
			fich.close();
		}catch(IOException e) {
			System.out.println("Erreur lors du chargement du fichier contenant le circuit");
		}
		return res;
	}
	
	/**
	 * Methode de demande d'un entier a l'utilisateur
	 * @param min Valeur minimale attendue
	 * @param max Valeur maximale attendue
	 * @return Valeur donnee par l'utilisateur
	 */
	public int demanderEntier(int min, int max) {
		String aff = String.format("Veuillez entrer un nombre entier entre %d et %d", min, max);
		System.out.println(aff);
		int nb = min-1;
		while(nb<min || nb>max) {
			try {
				nb = sc.nextInt();
				if(nb<min || nb>max) System.out.println("Veuillez entrer un nombre entre "+min+" et "+max);
			}catch(InputMismatchException e) {
				System.out.println("La valeur que vous avez entree n'est pas entiere. Veuillez reessayer : ");
				sc.nextLine();
				nb = min-1;
			}
		}
		return nb;
	}
	
	/**
	 * Methode de demande d'une chaine de caractere a l'utilisateur
	 * @return Chaine de caractere donnee par l'utilisateur
	 */
	public String demanderString() {
		return sc.next();
	}
	
	/**
	 * Methode de fermeture du flux du Scanner reste ouvert pendant l'execution du programme
	 */
	public void fermer() {
		sc.close();
	}
}
