package perso;

/**
 * Classe representant les Rouleurs dans le jeu
 * @author Gaetan Henry, Ulysse Kaba
 */
public class Rouleur extends Cycliste{
	
	/**
	 * Constructeur simple
	 * Fait appel au constructeur de Cycliste
	 */
	public Rouleur() {
		super("Rouleur");
		try{
			setCartes(new Deck("rouleur"));
		} catch(ErreurTypeInconnu e) {
			System.out.println("Impossible de creer le deck de cartes du rouleur");
			e.printStackTrace();
		}
	}
	
}
