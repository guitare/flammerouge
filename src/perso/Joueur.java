package perso;

import java.util.ArrayList;

/**
 * Classe representant un joueur
 * @author Gaetan Henry, Ulysse Kaba
 */
public class Joueur {
	private String nom;
	private Rouleur rlr;
	private Sprinter spr;
	private Carte joueeR, joueeS;
	
	/**
	 * Constructeur d'un joueur
	 * @param n Nom du joueur a creer
	 */
	public Joueur(String n) {
		this.nom = n;
		this.rlr = new Rouleur();
		this.spr = new Sprinter();
	}
	
	/**
	 * Methode d'avancement du Rouleur du joueur
	 * @return vrai si le deplacement a eu lieu, faux sinon
	 */
	public boolean avancerR() {
		boolean fini=false;
		boolean montee=false;
		boolean descente=false;
		int avance=joueeR.getVal();
		for(int i=rlr.getX();i<=rlr.getX()+joueeR.getVal();i++){
			if(i<Jeu.circ.getTrace().length) {
				if(Jeu.circ.getDeniv()[i].compareTo("Montee")==0) {
					montee=true;
				}else if(Jeu.circ.getDeniv()[i].compareTo("Descente")==0){
					descente=true;
				}
			}
		}
		if(montee && avance>5) {
			avance=5;
		}else if(descente && avance<5) {
			avance=5;
		}
		
		if(rlr.avancer(avance)){
			fini=true;
		}
		
		return fini;
	}

	/**
	 * Methode d'avancement du Sprinter du joueur
	 * @return vrai si le deplacement a eu lieu, faux sinon
	 */
	public boolean avancerS() {
		boolean fini=false;
		boolean montee=false;
		boolean descente=false;
		int avance=joueeR.getVal();
		for(int i=rlr.getX();i<=rlr.getX()+joueeR.getVal();i++){
			if(i<Jeu.circ.getTrace().length) {
				if(Jeu.circ.getDeniv()[i].compareTo("Montee")==0) {
					montee=true;
				}else if(Jeu.circ.getDeniv()[i].compareTo("Descente")==0){
					descente=true;
				}
			}
		}
		if(montee && avance>5) {
			avance=5;
		}else if(descente && avance<5) {
			avance=5;
		}
		
		if(spr.avancer(joueeS.getVal())){
			fini=true;
		}
		return fini;
	}
	
	/**
	 * Methode de pioche dans le Deck du Rouleur du joueur
	 */
	public void piocherR() {
		rlr.piocher();
	}
	/**
	 * Methode de pioche dans le Deck du Sprinter du joueur
	 */
	public void piocherS() {
		spr.piocher();
	}
	
	
	/**
	 * Methode de placement du Rouleur du joueur
	 * @param i Abscisse du cycliste a placer
	 * @return vrai si le placement a eu lieu, faux sinon
	 */
	public boolean placerR(int i) {
		boolean res=false;
		if(rlr.placer(i)){
			res=true;
		}
		return res;
	}
	/**
	 * Methode de placement du Sprinter du joueur
	 * @param i Abscisse du cycliste a placer
	 * @return vrai si le placement a eu lieu, faux sinon
	 */
	public boolean placerS(int i) {
		boolean res=false;
		if(spr.placer(i)){
			res=true;
		}
		return res;
	}
	
	/**
	 * Methode de selection d'une carte du Rouleur
	 * @param i Index de la carte a choisir dans la main
	 */
	public void selectionnerR(int i) {
		this.joueeR = rlr.selectionner(i);
	}
	/**
	 * Methode de selection d'une carte du Sprinter
	 * @param i Index de la carte a choisir dans la main
	 */
	public void selectionnerS(int i) {
		this.joueeS = spr.selectionner(i);
	}
	
	
	
	// Getters & Setters
	
	/**
	 * Getter de la derniere carte jouee par le Rouleur
	 * @return Derniere carte jouee
	 */
	public Carte getJoueeR() {
		return this.joueeR;
	}
	/**
	 * Getter de la derniere carte jouee par le Sprinter
	 * @return Derniere carte jouee
	 */
	public Carte getJoueeS() {
		return this.joueeS;
	}
	/**
	 * Getter de la main du Rouleur
	 * @return ArrayList de cartes representant la main du Rouleur
	 */
	public ArrayList<Carte> getMainR(){
		return rlr.getMain();
	}
	/**
	 * Getter de la main du Sprinter
	 * @return ArrayList de cartes representant la main du Sprinter
	 */
	public ArrayList<Carte> getMainS(){
		return spr.getMain();
	}
	/**
	 * Getter du nom du Joueur
	 * @return Nom du joueur
	 */
	public String getNom() {
		return this.nom;
	}
}
