package perso;

import java.util.ArrayList;

/**
 * Classe representant les cyclistes dans le jeu
 * @author Gaetan Henry, Ulysse Kaba
 */
public class Cycliste {
	private String type;
	protected Deck cartes;
	private int x;
	private String nom;
	private static int numS=1;
	private static int numR=1;
	
	/**
	 * Constructeur utilise dans les classes heritant de cette classe
	 * @param t Type du cycliste cree
	 */
	public Cycliste(String t) {
		if(t.equals("Sprinter")){
			nom="S"+numS;
			numS+=1;
			this.type = t;
		}else if(t.equals("Rouleur")){
			nom="R"+numR;
			numR+=1;
			this.type = t;
		}
	}
	
	/**
	 * Methode de placement du cycliste en debut de partie
	 * @param i Abscisse voulue du cycliste
	 * @return Vrai si le placement a eu lieu, faux sinon
	 */
	public boolean placer(int i) {
		boolean res=false;
		if(i>=0) {
			if(Jeu.circ.estLibre(i)){
				res=Jeu.circ.placerPremier(this,i);
				this.x=i;
			}
		}
		return res;
	}
	
	/**
	 * Methode pour faire avancer le cycliste sur le circuit
	 * @param nb Nombre de cases a avancer
	 * @return vrai si le cycliste arrive au bout du circuit, faux sinon
	 */
	public boolean avancer(int nb) {
		boolean fini=false;
		int fin=this.x + nb;
		if(fin<Jeu.circ.getTrace().length){
			while(!Jeu.circ.estLibre(this.x+nb)) {
				nb--;
			}
			Jeu.circ.placerCycliste(this, this.x+nb);
			Jeu.circ.liberer(this);
			this.x += nb;
		}else{
			fini=true;
		}
		return fini;
	}
	
	/**
	 * Methode d'application de la fatigue sur un cycliste
	 */
	public void fatiguer() {
		cartes.ajouterFatigue();
	}
	
	/**
	 * Methode de pioche de cartes
	 * Utilise la methode piocher de la classe Deck
	 */
	public void piocher() {
		cartes.piocher();
	}
	/**
	 * Methode de selection d'une des cartes de la main du joueur
	 * @param i Index de la carte a selectionner
	 * @return Carte choisie par le joueur
	 * Utilise la methode selectionner de la classe Deck
	 */
	public Carte selectionner(int i) {
		return cartes.selectionner(i);
	}
	
	
	
	// Getters & Setters
	
	/**
	 * Getter de la main du cycliste
	 * @return ArrayList de Cartes correspondant a la main du cycliste
	 */
	public ArrayList<Carte> getMain(){
		return cartes.getMain();
	}
	/**
	 * Setter des cartes du cyliste executant la methode
	 * @param c Deck de cartes a donner au cycliste
	 */
	public void setCartes(Deck c) {
		this.cartes = c;
	}
	/**
	 * Getter des cartes du cycliste executant la methode
	 * @return Deck de cartes du cycliste
	 */
	public Deck getCartes() {
		return this.cartes;
	}
	/**
	 * Getter de l'abscisse du cycliste
	 * @return Abscisse du cycliste executant la methode
	 */
	public int getX() {
		return this.x;
	}
	/**
	 * Getter du type du cycliste
	 * @return Chaine de caractere correspondant au type du cycliste
	 */
	public String getType() {
		return this.type;
	}
	
	
	/**
	 * Methode toString du cycliste
	 * @return Chaine de caractere representant le type du cycliste
	 */
	public String toString() {
		return nom;
	}
}
