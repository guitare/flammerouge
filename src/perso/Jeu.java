package perso;
import java.util.*;

/**
 * Classe representant le jeu
 * @author Gaetan Henry, Ulysse Kaba
 */
public class Jeu {
	
	public static Circuit circ;
	
	private int nbJ;
	private Joueur[] joueurs;
	private boolean fini;
	private Interactions inter;
	
	/**
	 * Constructeur du jeu
	 * Sert en particulier � initialiser le circuit
	 */
	public Jeu() {
		fini = false;
		inter = new Interactions("derniere_partie.txt");
		
		// intialisation du circuit a partir du fichier "circuit.txt"
		// si �a marche pas on prend le circuit par defaut
		try{
			circ = new Circuit(inter.chargerCircuit("circuit.txt"));
		}catch(Error e) {
			System.out.println("Le circuit demande est trop petit, utilisation du circuit par defaut.");
			circ = new Circuit();
		}
	}
	
	/**
	 * M�thode d'initialisation du jeu
	 * Demande � l'utilisateur le nombre de joueurs
	 * Et initialise chacun des joueurs selon les informations fournies
	 */
	public void initialiser() {
		System.out.println("Nombre de joueurs :");
		
		// demande du nombre de joueurs
		nbJ = inter.demanderEntier(2, 4);
		
		joueurs = new Joueur[nbJ];
		
		// initialisation de chacun des joueurs
		for(int i=0; i<nbJ; i++) {
			//choix nom du joueur
			System.out.println("Veuillez entrer le nom du joueur n�"+(i+1));
			String nom = inter.demanderString();
			joueurs[i] = new Joueur(nom);
			
			// affichage position autres joueurs
			System.out.println("Voici la disposition des autres joueurs sur la ligne de depart : ");
			System.out.println(Jeu.circ.departToString());
			
			// choix positions de depart du rouleur
			System.out.println(nom+", dans quelle case souhaitez vous placer votre rouleur ?");
			int place = inter.demanderEntier(1, 5);
			while(!joueurs[i].placerR(place-1)) {
				System.out.println("Cette place est deja prise");
				place = inter.demanderEntier(1, 5);
			}
			
			//choix position de depart du sprinter
			System.out.println(nom+", dans quelle case souhaitez vous placer votre sprinter ?");
			place = inter.demanderEntier(1, 5);
			while(!joueurs[i].placerS(place-1)) {
				System.out.println("Cette place est deja prise");
				place = inter.demanderEntier(1, 5);
			}
		}
		
		//affichage de l'etat du jeu apres initialisation des joueurs
		String affichage = "Voici la disposition des joueurs en debut de jeu :\n"+Jeu.circ.departToString();
		System.out.println(affichage);
		inter.ecrire(affichage);
		
	}
	
	/**
	 * Methode de lancement du jeu
	 * Tourne en boucle tant qu'aucun joueur n'a passe la ligne d'arrivee
	 */
	public void lancer() {
		
		int tour = 0; //permet d'identifer le tour actuel du jeu
		String gagnant = null; //nom du joueur gagnant
		while(!fini) {
			tour++;
			System.out.println("\n\nVous passez au tour numero : "+tour);
			inter.ecrire("\n\nTour numero : "+tour);
			
			// phase d'energie
			for(int i=0; i<nbJ; i++) {
				Joueur j = joueurs[i]; // joueur traite par la boucle
				
				// affichage etat du jeu
				System.out.println("\nPour rappel, voici l'avencement actuel du jeu : \n"+Jeu.circ);
				System.out.println("Au tour de "+j.getNom()+" de jouer, quel cycliste voulez vous jouer en premier ? (1:rouleur/2:sprinteur)");
				
				// selection du cycliste a jouer en premier
				int cycl = inter.demanderEntier(1, 2);
				
				// ordre des actions suivant le choix du joueur
				if(cycl==1) {
					jouerRouleur(j);
					jouerSprinter(j);
				}else {
					jouerSprinter(j);
					jouerRouleur(j);
				}
			}
			
			// phase de mouvement
			
			// sauvegarde de l'etat du jeu avant deplacement
			inter.ecrire("etat du jeu avant deplacement des cyclistes :\n"+Jeu.circ);
			
			//revelation des cartes choisies et deplacement des cyclistes
			String t = "\nLes cartes jouees par les joueurs sont : ";
			System.out.println(t);
			inter.ecrire(t);
			
			for(int i=0; i<nbJ; i++) {
				Joueur j = joueurs[i];
				if(!fini){
					t = String.format("Le joueur %s a choisi la carte de valeur %d pour son rouleur et %d pour son sprinter", j.getNom(), j.getJoueeR().getVal(), j.getJoueeS().getVal());
					System.out.println(t);
					inter.ecrire(t);

					// deplacement des cyclistes
					if(j.avancerR()) {
						fini=true;
					}
					if(j.avancerS()) {
						fini=true;
					}
				}
				// si un joueur a gagne la partie en depassant la ligne d'arrivee
				if(fini){
					gagnant=j.getNom();
				}
			}

			aspiration(); // applique l'aspiration aux cyclistes
			fatigue(); // applique la fatigue aux cyclistes
			
			//affichage etat a la fin du tour
			t = "\netat du jeu apres le deplacement des cyclistes :\n"+Jeu.circ+"\nFin du tour n�"+tour+"\n";
			System.out.println(t);
			inter.ecrire(t);

		}
		
		// Quand l'un des cyclistes a passe la ligne d'arrivee
		String t = "Le joueur "+gagnant+" a gagne cette partie de Flamme Rouge au bout de "+tour+" tours";
		System.out.println(t);
		inter.ecrire(t);
		inter.fermer(); // permet de fermer le scanner utilise dans la classe interactions
	}
	
	
	
	/**
	 * Methode pour faire jouer le rouleur d'un joueur
	 * @param j Joueur en train de jouer
	 */
	public void jouerRouleur(Joueur j) {
		// Le joueur pioche 4 cartes parmis celles de son Rouleur
		j.piocherR();
		ArrayList<Carte> main = j.getMainR();
		// affichage cartes piochees
		System.out.print("Voici les cartes que vous avez piochees pour votre rouleur : ");
		for(int i=0; i<main.size();i++) {
			System.out.print(main.get(i)+" ");
		}
		// selection d'une carte
		System.out.println("\nVoulez vous jouer la 1ere, la 2eme, la 3eme ou la 4eme carte ?");
		int id = inter.demanderEntier(1, 4);
		j.selectionnerR(id-1);
	}
	/**
	 * Methode pour faire jouer le Sprinter d'un joueur
	 * @param j Joueur en train de jouer
	 */
	public void jouerSprinter(Joueur j) {
		// Le joueur pioche 4 cartes parmis celles de son Sprinter
		j.piocherS();
		ArrayList<Carte> main = j.getMainS();
		// affichage cartes piochees
		System.out.print("Voici les cartes que vous avez piochees pour votre sprinter : ");
		for(int i=0; i<main.size();i++) {
			System.out.print(main.get(i)+" ");
		}
		// selection d'une carte
		System.out.println("\nVoulez vous jouer la 1ere, la 2eme, la 3eme ou la 4eme carte ?");
		int id = inter.demanderEntier(1, 4);
		j.selectionnerS(id-1);
	}
	
	
	
	/**
	 * Methode d'application de l'aspiration sur les cyclistes
	 */
	public void aspiration(){
		while(circ.aspirationPossible()){
			for(int i=0;i<circ.getTrace().length-2;i++){
				if(circ.getTrace()[i][0]!=null && circ.getTrace()[i+1][0]==null && circ.getTrace()[i+2][0]!=null){
					circ.getTrace()[i+1][0]=circ.getTrace()[i][0];
					circ.getTrace()[i][0]=null;
					if(circ.getTrace()[i][1]!=null){
						circ.getTrace()[i][0]=circ.getTrace()[i][1];
						circ.getTrace()[i][1]=null;
					}
				}
			}
		}
	}
	/**
	 * Methode d'application de la fatigue sur les cyclistes
	 */
	public void fatigue() {
		for(int i=0; i<circ.getTrace().length-1; i++) {
			if(circ.getTrace()[i][0]!=null && circ.getTrace()[i+1][0]==null) {
				circ.getTrace()[i][0].fatiguer();
			}
			if(circ.getTrace()[i][1]!=null && circ.getTrace()[i+1][1]==null) {
				circ.getTrace()[i][1].fatiguer();
			}
		}
	}
}
