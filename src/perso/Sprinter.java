package perso;

/**
 * Classe representant les Sprinters dans le jeu
 * @author Gaetan Henry, Ulysse Kaba
 */
public class Sprinter extends Cycliste{
	
	/**
	 * Constructeur simple
	 * Fait appel au constructeur de Cycliste
	 */
	public Sprinter() {
		super("Sprinter");
		try{
			setCartes(new Deck("sprinter"));
		} catch(ErreurTypeInconnu e) {
			System.out.println("Impossible de creer le deck de cartes du sprinter");
			e.printStackTrace();
		}
	}
}
