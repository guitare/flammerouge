package test;

import static org.junit.Assert.*;

import org.junit.Test;

import perso.Carte;

public class CarteTest {

	@Test
	public void testConstructeurNormal() {
		Carte test=new Carte(5);
		assertEquals("la valeur de la carte devrais �tre 5",5,test.getVal());
	}
	
	@Test
	public void testConstructeurNull() {
		Carte test=new Carte(0);
		assertEquals("la valeur de la carte devrais �tre 0",0,test.getVal());
	}
	
	@Test
	public void testConstructeurN�gatif() {
		Carte test=new Carte(-5);
		assertEquals("la valeur de la carte devrais �tre 0",0,test.getVal());
	}
	
	@Test
	public void testToString() {
		Carte test=new Carte(5);
		String aff=test.toString();
		assertEquals("la chaine devrais etre \"5\" ","5",aff);
	}
	
	
}
