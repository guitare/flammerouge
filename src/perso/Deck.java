package perso;
import java.util.*;

/**
 * Classe representant les Deck des cyclistes
 * Contient des cartes posees sur la table
 * Contient des cartes tenues dans la main lors du choix
 * Contient des cartes defaussees
 * @author Gaetan Henry, Ulysse Kaba
 */
public class Deck {
	private ArrayList<Carte> cartes;
	private ArrayList<Carte> main;
	private ArrayList<Carte> defausse;
	
	/**
	 * Constructeur du Deck
	 * @param type Chaine de caractere precisant le type du cycliste dont on veut generer le Deck
	 * @throws ErreurTypeInconnu Soulevee lorsque le type passe en parametre ne correspond pas aux types existants
	 */
	public Deck(String type) throws ErreurTypeInconnu {
		cartes = new ArrayList<Carte>();
		main = new ArrayList<Carte>();
		defausse = new ArrayList<Carte>();
		if(type.equals("rouleur")) genererRouleur();
		else if (type.equals("sprinter")) genererSprinter();
		else throw new ErreurTypeInconnu();
	}
	/**
	 * Methode de generation du Deck d'un rouleur
	 */
	private void genererRouleur() {
		for(int i=3; i<=7; i++) {
			for(int j=0; j<3; j++) {
				cartes.add(new Carte(i));
			}
		}
	}
	/**
	 * Methode de generation du Deck d'un sprinter
	 */
	private void genererSprinter() {
		for(int i=2; i<=5; i++) {
			for(int j=0; j<3; j++) {
				cartes.add(new Carte(i));
			}
		}
		for(int i=0; i<3; i++) {
			cartes.add(new Carte(9));
		}
	}
	
	
	/**
	 * Methode d'application de la fatigue sur le cycliste
	 * (Rajoute une carte fatigue dans le jeu du cycliste)
	 */
	public void ajouterFatigue() {
		cartes.add(new Carte(2));
	}
	/**
	 * Methode de melange des cartes du cycliste 
	 * (appelee avant chaque pioche)
	 * (appliquee uniquement sur les cartes posees sur la table)
	 */
	public void melanger() {
		Collections.shuffle(cartes);
	}
	
	
	
	/**
	 * Methode de pioche de 4 cartes
	 */
	public void piocher() { //pioche trois cartes dans le deck pour les proposer au joueur
		if(cartes.size()<4) {
			for(int i=0; i<defausse.size(); i++) {
				cartes.add(defausse.get(0));
				defausse.remove(0);
			}
		}
		melanger();
		int max = 4;
		if(cartes.size()<max) max = cartes.size();
		for(int i=0; i<max; i++) {
			main.add(cartes.get(0));
			cartes.remove(0);
		}
	}

	/**
	 * Methode de selection d'une carte parmis les cartes de la main
	 * @param p index dans la main de la carte a selectionner
	 * @return Carte choisie par le joueur
	 */
	public Carte selectionner(int p) { //selectionne la carte voulue par le joueur et repose les autres dans la defausse
		Carte temp = main.get(p);
		main.remove(p);
		while(main.size()>0) {
			defausse.add(main.get(0));
			main.remove(0);
		}
		return temp;
		
	}
	
	
	/**
	 * Getter de la main du cycliste
	 * @return ArrayList de cartes correspondant aux cartes tenues en main lors du choix
	 */
	public ArrayList<Carte> getMain(){ //retourne la main du joueur pour lui demander quelle carte choisir
		return this.main;
	}
	
	public ArrayList<Carte> getCartes(){ //retourne le paquet du joueur pour lui demander quelle carte choisir
		return this.cartes;
	}
	
	public ArrayList<Carte> getDefausse(){ //retourne la defausse du joueur pour lui demander quelle carte choisir
		return this.defausse;
	}
}
